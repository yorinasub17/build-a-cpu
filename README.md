# build-a-cpu

This repository contains the source code for exercises pertaining to learning how to build a CPU from scratch.


## Overview

The target CPU built is based on the book
["CPUの創りかた"](https://www.amazon.co.jp/CPU%E3%81%AE%E5%89%B5%E3%82%8A%E3%81%8B%E3%81%9F-%E6%B8%A1%E6%B3%A2-%E9%83%81/dp/4839909865),
implementing the components in VHDL for deployment on to the FPGA in the Basys 3 development board.
